--- 模块功能：tcpServer功能，需要外挂ch395q模块

-- @author openLuat
-- @module socketLongConnection.testSocket1
-- @license MIT
-- @copyright openLuat
-- @release 2021.11.20

module(..., package.seeall)

require 'socket'


local ready = false

--- socket连接是否处于激活状态
-- @return 激活状态返回true，非激活状态返回false
-- @usage socketTask.isReady()
function isReady()
    return ready
end
local a1 = {}

--启动socket客户端任务
sys.taskInit(
    function()
        local retryConnectCnt = 0
        while true do
            if not socket.isReady() then
                retryConnectCnt = 0
                --等待网络环境准备就绪，超时时间是5分钟
                sys.waitUntil('IP_READY_IND', 300000)
            end
            if socket.isReady() then
                --创建一个socket tcpServer
                local socketClient = socket.tcpServer()
                --阻塞执行socket connect动作，直至成功
                if socketClient:foundserver('34508', 600000) then
                    --if socketClient:connect("112.125.89.8","34123") then
                    retryConnectCnt = 0
                    ready = true
                    local ret = socket.isReadyEth()
                    log.info('server IP :', ret[2][2])
                    --循环处理接收和发送的数据
                    while true do
                        sys.wait(500)
                        if not socket.isReady() then
                            break
                        end
                    end
                    ready = false
                else
                    retryConnectCnt = retryConnectCnt + 1
                end
                --断开socket连接
                log.info('关闭')
                socketClient:close()
                if retryConnectCnt >= 5 then
                    link.shut()
                    retryConnectCnt = 0
                end
                sys.wait(5000)
            else
                --进入飞行模式，20秒之后，退出飞行模式
                net.switchFly(true)
                sys.wait(20000)
                net.switchFly(false)
            end
        end
    end
)




--默认拉低了以太网模块TX脚，没拉低，自行拉低
-- local setGpioFnc_TX = pins.setup(gpio, 0)
sys.taskInit(function() 
    if     link.CutNetwork(2,pio.P0_22,pio.P0_23,function (id, msg, dat)
        if id==26 then
            log.info('PHY', msg, dat)
        end
        if id==42 then
            log.info('DHCP', msg, dat)
        end

        if id==303  then
            log.info('有客户端连接:'..dat[1],'IP:'..dat[2]..'PORT:'..dat[3])
            table.insert(a1,dat)
        end
        if id==301  then
            log.info('往客户端'..msg..'发送成功')
        end
        if id==302  then
            log.info('客户端'..msg..'收到消息：',dat[1],dat[2])
        end
        if id==304  then
            if dat then
                log.info('客户端'..msg..'关闭成功')
            end
        end
    end) then
        log.info("切换以太网成功")
    else
        log.info("切换以太网失败")
    end

end)


sys.taskInit(function ()
    while true do
        sys.wait(5000)
        if #a1>0 then
            for i=1,#a1 do
                socket.sendETH(a1[i][1],'Hello World!')
            end
        end
    end
end)



-- sys.taskInit(function() 
--     while true do
--         sys.wait(60000)
--         socket.closeEth(1)
--     end
-- end)