--- 模块功能：数据链路激活、SOCKET管理(创建、连接、数据收发、状态维护)
-- @module socket
-- @author openLuat
-- @license MIT
-- @copyright openLuat
-- @release 2017.9.25
require 'link'
require 'utils'
module(..., package.seeall)

local sockets = {}
-- 单次发送数据最大值
local SENDSIZE = 11200
--以太网单次最大值（2k）
local SENDSIZE_ETH = 4096
--以太网接收缓存（1k）

-- 缓冲区最大下标
local INDEX_MAX = 256
-- 是否有socket正处在链接
local socketsConnected = 0
--- SOCKET 是否有可用
-- @return 可用true,不可用false
socket.isReady = link.isReady

local function errorInd(error)
    local coSuspended = {}

    for _, c in pairs(sockets) do -- IP状态出错时，通知所有已连接的socket
        c.error = error
        --不能打开如下3行代码，IP出错时，会通知每个socket，socket会主动close
        --如果设置了connected=false，则主动close时，直接退出，不会执行close动作，导致core中的socket资源没释放
        --会引发core中socket耗尽以及socket id重复的问题
        --c.connected = false
        --socketsConnected = c.connected or socketsConnected
        --if error == 'CLOSED' then sys.publish("SOCKET_ACTIVE", socketsConnected) end
        if c.co and coroutine.status(c.co) == 'suspended' then
            --coroutine.resume(c.co, false)
            table.insert(coSuspended, c.co)
        end
    end
    for k, v in pairs(coSuspended) do
        if v and coroutine.status(v) == 'suspended' then
            coroutine.resume(v, false, error)
        end
    end
end

sys.subscribe(
    'IP_ERROR_IND',
    function()
        errorInd('IP_ERROR_IND')
    end
)
--sys.subscribe('IP_SHUT_IND', function()errorInd('CLOSED') end)
-- 创建socket函数
local mt = {}
mt.__index = mt
local function socket(protocol, cert, tCoreExtPara)
    local ssl = protocol:match('SSL')
    local co = coroutine.running()
    if not co then
        log.warn('socket.socket: socket must be called in coroutine')
        return nil
    end
    -- 实例的属性参数表
    local o = {
        id = nil,
        --类型
        protocol = protocol,
        tCoreExtPara = tCoreExtPara,
        ssl = ssl,
        cert = cert,
        co = co,
        input = {},
        output = {},
        wait = '',
        connected = false,
        iSubscribe = false,
        subMessage = nil,
        isBlock = false,
        msg = nil,
        rcvProcFnc = nil,
        --tcpserver 是否被创建
        found = false
    }
    return setmetatable(o, mt)
end

--- 创建基于TCP的socket对象
-- @bool[opt=nil] ssl，是否为ssl连接，true表示是，其余表示否
-- @table[opt=nil] cert，ssl连接需要的证书配置，只有ssl参数为true时，此参数才有意义，cert格式如下：
-- {
--     caCert = "ca.crt", --CA证书文件(Base64编码 X.509格式)，如果存在此参数，则表示客户端会对服务器的证书进行校验；不存在则不校验
--     clientCert = "client.crt", --客户端证书文件(Base64编码 X.509格式)，服务器对客户端的证书进行校验时会用到此参数
--     clientKey = "client.key", --客户端私钥文件(Base64编码 X.509格式)
--     clientPassword = "123456", --客户端证书文件密码[可选]
-- }
-- @return client，创建成功返回socket客户端对象；创建失败返回nil
-- @usage
-- c = socket.tcp()
-- c = socket.tcp(true)
-- c = socket.tcp(true, {caCert="ca.crt"})
-- c = socket.tcp(true, {caCert="ca.crt", clientCert="client.crt", clientKey="client.key"})
-- c = socket.tcp(true, {caCert="ca.crt", clientCert="client.crt", clientKey="client.key", clientPassword="123456"})
function tcp(ssl, cert, tCoreExtPara)
    return socket('TCP' .. (ssl == true and 'SSL' or ''), (ssl == true) and cert or nil, tCoreExtPara)
end

--- 创建基于UDP的socket对象
-- @return client，创建成功返回socket客户端对象；创建失败返回nil
-- @usage c = socket.udp()
function udp()
    return socket('UDP')
end
--- 创建基于TCP server的socket对象
-- @return client，创建成功返回socket客户端对象；创建失败返回nil
-- @usage c = socket.udp()
function tcpServer()
    return socket('TcpServer')
end
-------
function str2short(str)
    f, n = pack.unpack(str, '<H')
    return f and f > 0 and n or 0
end
gfunc = function(id, msg, dat)
end
local function shorts(n)
    return pack.pack('<H', n)
end

local function sub_z(data, num)
    return string.sub(data, num + 1, data:len())
end
function ip2num(str)
    if (not (str and #str == 4)) then
        return ''
    end
    local t = {pack.unpack(str, 'bbbb')}
    return string.format('%d.%d.%d.%d', t[2], t[3], t[4], t[5])
end
local function iptostr(ip)
    local d1, d2, d3, d4 = string.match(ip, '(%d+)%.(%d+)%.(%d+).(%d+)')
    return {
        string.sub(shorts(d1), 1, 1),
        string.sub(shorts(d2), 1, 1),
        string.sub(shorts(d3), 1, 1),
        string.sub(shorts(d4), 1, 1)
    }
end
--已被连接
socket_connect = {}
--已开启socket数组
socket_on = {}
--未开启socket数组(索引，{IP，端口}，端口，协议类型)
socket_off = {
    {'\x00', {nil, nil}, nil, nil},
    {'\x01', {nil, nil}, nil, nil},
    {'\x02', {nil, nil}, nil, nil},
    {'\x03', {nil, nil}, nil, nil},
    {'\x04', {nil, nil}, nil, nil},
    {'\x05', {nil, nil}, nil, nil},
    {'\x06', {nil, nil}, nil, nil},
    {'\x07', {nil, nil}, nil, nil}
}
tcpservert = {}
serverid = nil
--创建socket(参数)
-- 0.tcp客户端
-- 1.udp客户端
-- 2.ssl(暂不支持)
-- 3.raw
-- 4.tcp 服务端
function socketEth(ty, IPC, portC)
    local portS
    if #socket_off == 0 or not (ty < 5) then
        return nil, 'no socket'
    end
    -- portS=math.random(0,65535)

    if ty == 4 and type(portC) ~= nil then
        portS = portC
    else
        portS = (str2short(socket_off[1][1] .. '\x00') + 123) * 5
    end
    local id = str2short(socket_off[1][1] .. '\x00')
    spi.send_recv(spi.SPI_1, '\x19\xff\xff')
    spi.send_recv(spi.SPI_1, '\x30\xff\xff')
    if ty == 4 then
        -- statements
        for i = 1, 8 do
            spi.send_recv(spi.SPI_1, '\x34' .. socket_off[1][1] .. string.char(3))
            spi.send_recv(spi.SPI_1, '\x33' .. socket_off[1][1] .. shorts(portS))
            if i == 1 then
                spi.send_recv(spi.SPI_1, '\x35' .. socket_off[1][1])
                serverid = socket_off[1][1]
                if sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1) == '\x00' then
                    socket_off[1][3] = portS
                    socket_off[1][4] = ty
                    table.insert(socket_on, table.remove(socket_off, 1))
                end
            else
                if sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1) == '\x00' then
                    log.info('创建成功', socket_off[1][1]:toHex())
                    socket_off[1][3] = portS
                    socket_off[1][4] = ty
                    table.insert(socket_on, table.remove(socket_off, 1))
                end
                if i == 8 then
                    spi.send_recv(spi.SPI_1, '\x36' .. serverid)
                    if sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1) == '\x00' then
                        log.info('监听成功', serverid)
                        return str2short(serverid .. '\x00')
                    end
                end
            end
        end
    else
        if ty == 0 then
            spi.send_recv(spi.SPI_1, '\x34' .. socket_off[1][1] .. string.char(3))
        elseif ty == 1 then
            spi.send_recv(spi.SPI_1, '\x34' .. socket_off[1][1] .. string.char(ty))
        end
        spi.send_recv(spi.SPI_1, '\x33' .. socket_off[1][1] .. shorts(portS))
        if ty == 0 or ty == 1 then
            spi.send_recv(spi.SPI_1, '\x32' .. socket_off[1][1] .. shorts(tonumber(portC)))
            local ipt = iptostr(IPC)
            spi.send_recv(spi.SPI_1, '\x31' .. socket_off[1][1] .. ipt[1] .. ipt[2] .. ipt[3] .. ipt[4])
            spi.send_recv(spi.SPI_1, '\x35' .. socket_off[1][1])
            spi.send_recv(spi.SPI_1, '\x37' .. socket_off[1][1])
        end
        local aa = sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1)
        if not aa == '\x00' then
            gtab.func()
            return nil
        end
        socket_off[1][3] = port
        socket_off[1][4] = ty
        table.insert(socket_on, table.remove(socket_off, 1))
        return id
    end
end


function wh()
    local a = sub_z(spi.send_recv(spi.SPI_1, '\x19\xff\xff'), 1)
    log.info('19', a:toHex())
    if bit.isset(str2short(a), 2) then
        -- PHY中断
        local a1 = sub_z(spi.send_recv(spi.SPI_1, '\x26\xff'), 1)
        if a1 == '\x01' then
            gfunc(26, false, 'phy false')
        elseif a1 == '\x08' then
            gfunc(26, true, 'phy true')
        end
    elseif bit.isset(str2short(a), 3) then
        --DHCP中断
        a1 = sub_z(spi.send_recv(spi.SPI_1, '\x42\xff'), 1)
        if a1 == '\x00' then
            gfunc(42, true, 'DHCP true')
        elseif a1 == '\x01' then
            gfunc(42, false, 'DHCP false')
        end
    elseif bit.isset(str2short(a), 0) then
        --不可达中断
        log.info('Unreachable interrupt')
    else
        local a1t = {'sending area nil', 'send succeed', 'rece not nil', 'tcp connect ', 'tcp break', 'Timeout'}
        for i = 4, 11 do
            if bit.isset(str2short(a), i) then
                a2 = spi.send_recv(spi.SPI_1, string.char(0x30, (i - 4), 0xff, 0xff))
                a1 = sub_z(a2, 2)
                if string.sub(a2, 3, 3) == '0x00' then
                    a2 = spi.send_recv(spi.SPI_1, string.char(0x30, (i - 4), 0xff, 0xff))
                    a1 = sub_z(a2, 2)
                end
                for q = 0, 6 do
                    if bit.isset(str2short(a1), q) then
                        if q == 2 then
                            local data_leng = sub_z(spi.send_recv(spi.SPI_1, string.char(0x3b, (i - 4), 0xff, 0xff)), 2)
                            local data =
                                sub_z(
                                spi.send_recv(
                                    spi.SPI_1,
                                    string.char(0x3c, i - 4) .. data_leng .. string.rep('\xff', str2short(data_leng))
                                ),
                                4
                            )
                            sys.publish(
                                'MSG_SOCK_RECV_IND',
                                {
                                    ['socket_index'] = i - 4,
                                    ['id'] = 31,
                                    ['result'] = 0,
                                    ['recv_len'] = str2short(data_leng),
                                    ['recv_data'] = data
                                }
                            )
                            gfunc(302, (i - 4), {str2short(data_leng),data})
                            spi.send_recv(spi.SPI_1, '\x19\xff\xff')
                            spi.send_recv(spi.SPI_1, string.char(0x30, (i - 4), 0xff, 0xff))
                        end
                        if q == 1 then
                            sys.publish(
                                'SOCK_SEND_CN',
                                {
                                    ['socket_index'] = i - 4,
                                    ['id'] = 32,
                                    ['result'] = 0
                                }
                            )
                            
                            gfunc(301,(i - 4),nil)
                            spi.send_recv(spi.SPI_1, string.char(0x30, (i - 4), 0xff, 0xff))
                            spi.send_recv(spi.SPI_1, '\x19\xff\xff')
                            spi.send_recv(spi.SPI_1, string.char(0x30, (i - 4), 0xff, 0xff))
                        end
                        if q == 3 then
                            for p = 1, #socket_on do
                                if
                                    str2short(socket_on[p][1] .. '\x00') == i - 4 and
                                        (socket_on[p][4] == 1 or socket_on[p][4] == 0)
                                then
                                    sys.publish(
                                        'SOCK_CONN_CNF',
                                        {
                                            ['socket_index'] = i - 4,
                                            ['id'] = 34,
                                            ['result'] = 0
                                        }
                                    )
                                    table.insert(socket_connect, socket_on[p])
                                elseif str2short(socket_on[p][1] .. '\x00') == i - 4 and socket_on[p][4] == 4 then
                                    local id = str2short(socket_on[p][1] .. '\x00')
                                    table.insert(socket_connect, socket_on[p])
                                    local data =
                                        string.sub(
                                        spi.send_recv(
                                            spi.SPI_1,
                                            string.char(0x2d, (i - 4), 0xff, 0xff, 0xff, 0xff, 0xff, 0xff)
                                        ),
                                        3,
                                        8
                                    )
                                    -- server
                                    gfunc(303, q, {id, ip2num(string.sub(data, 1, 4)), str2short(string.sub(data, 5, 6))})
                                    sys.publish(
                                        'SOCK_CONN_CNF',
                                        {
                                            ['socket_index'] = i - 4,
                                            ['id'] = 34,
                                            ['result'] = 0
                                        }
                                    )
                                end
                            end
                        end
                        if q == 4 then
                            for o = 1, #socket_connect do
                                if str2short(socket_connect[o][1] .. '\x00') == i - 4 then
                                    table.remove(socket_connect, o)
                                    for w = 1, #socket_on do
                                        if str2short(socket_on[w][1] .. '\x00') == i - 4 then
                                            sys.publish(
                                                'MSG_SOCK_CLOSE_IND',
                                                {
                                                    ['socket_index'] = i - 4,
                                                    ['id'] = 33,
                                                    ['result'] = 0
                                                }
                                            )
                                            gfunc(304,(i - 4),true)
                                            table.insert(socket_off, table.remove(socket_on, w))
                                            break
                                        end
                                    end
                                    break
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

local a1 = {'42ff', '43ffffffffffffffffffffffffffffffffffffffff'}
--获取当前ch395状态及ip()
function isReadyEth()
    local isReadyt = {{false, ''}, {false, ''}, socket_connect, socket_on, socket_off}
    local a2 = string.sub(spi.send_recv(spi.SPI_1, string.fromHex(a1[1])), 2, 2)
    if link.ch395_mac == nil then
        isReadyt[1][1] = false
        isReadyt[1][2] = 'DHCP err'
        return isReadyt
    end
    isReadyt[1][1] = true
    isReadyt[1][2] = 'DHCP succeed'
    a2 = string.sub(spi.send_recv(spi.SPI_1, string.fromHex(a1[2])), 2, 21)
    if string.sub(a2, 1, 4) == '\xff\xff\xff\xff' or string.sub(a2, 1, 4) == '\x00\x00\x00\x00' then
        isReadyt[2][1] = false
        isReadyt[2][2] = 'ip err'
        return isReadyt
    end
    isReadyt[2][1] = true
    isReadyt[2][2] = ip2num(string.sub(a2, 1, 4))
    return isReadyt
end

--关闭socket，需重重新初始化
function closeEth(id)
    for o = 1, #socket_connect do
        if str2short(socket_connect[o][1] .. '\x00') == id then
            spi.send_recv(spi.SPI_1, string.char(0x38, id))
            local a = sub_z(sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1), 2)
            if a == '\x00' then
                log.info('close socket true')
            end
            table.remove(socket_connect, o)
            for w = 1, #socket_on do
                if str2short(socket_on[w][1] .. '\x00') == id then
                    sys.publish(
                        'SOCK_CLOSE_CNF',
                        {
                            ['socket_index'] = socket_on[w][1],
                            ['id'] = 33,
                            ['result'] = 0
                        }
                    )
                    table.insert(socket_off, table.remove(socket_on, w))
                    return true, 'close true'
                end
            end
            break
        end
    end
end

--发送数据(参数1.socket id 2.数据)
function sendETH(id, data)
    spi.send_recv(spi.SPI_1, '\x39' .. string.char(id) .. shorts(#data) .. data)
    --spi.send_recv(spi.SPI_1, string.char(0x30, id, 0xff, 0xff))
end

--- 创建服务器
-- @string address 服务器地址，支持ip和域名
-- @param port string或者number类型，服务器端口
-- @number[opt=120] timeout 可选参数，连接超时时间，单位秒
-- @return bool result true - 成功，false - 失败
-- @return string ,id '0' -- '8' ,返回通道ID编号
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
function mt:foundserver(port, timeout)
    assert(self.co == coroutine.running(), 'socket:found: coroutine mismatch')
    if not link.isReady() then
        log.info('socket.found: ip not ready')
        return false
    elseif link.ch395_mac == nil then
        log.info('socket.found: ETH not ready')
        return false
    elseif self.protocol ~= 'TcpServer' then
        log.info('socket.found: protocol is not tcpServer')
        return false
    end
    self.port = port
    local tCoreExtPara = self.tCoreExtPara or {}
    -- 默认缓冲区大小
    local rcvBufferSize = tCoreExtPara.rcvBufferSize or 0
    self.id = socketEth(4, nil, port, rcvBufferSize)
    if not self.id then
        log.info('socket:found: found tcpserver  error', self.protocol, address, port, self.cert)
        return false
    end
    log.info('socket:found-coreid,type,port,cert,timeout', self.id, self.protocol, port, self.cert, timeout or 120)
    sockets[self.id] = self
    self.wait = 'TCPSERVER_FOUND'
    -- self.timerId = sys.timerStart(coroutine.resume, (timeout or 120) * 1000, self.co, false, 'TIMEOUT')
    -- local result, reason = coroutine.yield()
    -- if self.timerId and reason ~= 'TIMEOUT' then
    --     sys.timerStop(self.timerId)
    -- end
    -- if not result then
    --     log.info('socket:found: found fail', reason)
    --     --sys.publish('LIB_SOCKET_CONNECT_FAIL_IND', self.ssl, self.protocol, address, port)
    --     return false
    -- end
    log.info('socket:found: found ok')
    if not self.found then
        self.found = true
    end
    return true, self.id
end
--- 连接服务器
-- @string address 服务器地址，支持ip和域名
-- @param port string或者number类型，服务器端口
-- @number[opt=120] timeout 可选参数，连接超时时间，单位秒
-- @return bool result true - 成功，false - 失败
-- @return string ,id '0' -- '8' ,返回通道ID编号
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
function mt:connect(address, port, timeout)
    assert(self.co == coroutine.running(), 'socket:connect: coroutine mismatch')
    if not link.isReady() then
        log.info('socket.connect: ip not ready')
        return false
    end
    self.address = address
    self.port = port
    local tCoreExtPara = self.tCoreExtPara or {}
    -- 默认缓冲区大小
    local rcvBufferSize = tCoreExtPara.rcvBufferSize or 0
    local socket_connect_fnc
    if link.ch395_mac ~= nil then
        --ethSocket.socket
        --socketEth
        socket_connect_fnc = socketEth
    else
        socket_connect_fnc =
            (type(socketcore.sock_conn_ext) == 'function') and socketcore.sock_conn_ext or socketcore.sock_conn
    end
    if self.protocol == 'TCP' then
        local d1, d2, d3, d4 = string.match(address, '(%d+)%.(%d+)%.(%d+).(%d+)')
        if not (d1 == nil or d2 == nil or d3 == nil or d4 == nil) then
            self.id = socket_connect_fnc(0, address, port, rcvBufferSize)
        else
            self.id = -2
        end
    elseif self.protocol == 'TcpServer' then
        if link.ch395_mac ~= nil then
            self.id = socket_connect_fnc(4, nil, port, rcvBufferSize)
        --self.serverLink=1
        end
    elseif self.protocol == 'TCPSSL' then
        local cert = {hostName = address}
        if self.cert then
            if self.cert.caCert then
                if self.cert.caCert:sub(1, 1) ~= '/' then
                    self.cert.caCert = '/lua/' .. self.cert.caCert
                end
                cert.caCert = io.readFile(self.cert.caCert)
            end
            if self.cert.clientCert then
                if self.cert.clientCert:sub(1, 1) ~= '/' then
                    self.cert.clientCert = '/lua/' .. self.cert.clientCert
                end
                cert.clientCert = io.readFile(self.cert.clientCert)
            end
            if self.cert.clientKey then
                if self.cert.clientKey:sub(1, 1) ~= '/' then
                    self.cert.clientKey = '/lua/' .. self.cert.clientKey
                end
                cert.clientKey = io.readFile(self.cert.clientKey)
            end
        end
        self.id = socket_connect_fnc(2, address, port, cert, rcvBufferSize)
    else
        self.id = socket_connect_fnc(1, address, port, rcvBufferSize)
    end
    if type(socketcore.sock_conn_ext) == 'function' then
        if not self.id or self.id < 0 then
            if self.id == -2 then
                require 'http'
                --请求腾讯云免费HttpDns解析
                log.info('进入解析')
                http.request(
                    'GET',
                    '119.29.29.29/d?dn=' .. address,
                    nil,
                    nil,
                    nil,
                    40000,
                    function(result, statusCode, head, body)
                        log.info('socket.httpDnsCb', result, statusCode, head, body)
                        sys.publish('SOCKET_HTTPDNS_RESULT_' .. address .. '_' .. port, result, statusCode, head, body)
                    end
                )
                local _, result, statusCode, head, body =
                    sys.waitUntil('SOCKET_HTTPDNS_RESULT_' .. address .. '_' .. port)

                --DNS解析成功
                if result and statusCode == '200' and body and body:match('^[%d%.]+') then
                    return self:connect(body:match('^([%d%.]+)'), port, timeout)
                end
            end
            self.id = nil
        end
    end
    if not self.id then
        log.info('socket:connect: core sock conn error', self.protocol, address, port, self.cert)
        return false
    end
    log.info(
        'socket:connect-coreid,prot,addr,port,cert,timeout',
        self.id,
        self.protocol,
        address,
        port,
        self.cert,
        timeout or 120
    )
    sockets[self.id] = self
    self.wait = 'SOCKET_CONNECT'
    self.timerId = sys.timerStart(coroutine.resume, (timeout or 120) * 1000, self.co, false, 'TIMEOUT')
    local result, reason = coroutine.yield()
    if self.timerId and reason ~= 'TIMEOUT' then
        sys.timerStop(self.timerId)
    end
    if not result then
        log.info('socket:connect: connect fail', reason)
        sys.publish('LIB_SOCKET_CONNECT_FAIL_IND', self.ssl, self.protocol, address, port)
        return false
    end
    log.info('socket:connect: connect ok')

    if not self.connected then
        self.connected = true
        socketsConnected = socketsConnected + 1
        sys.publish('SOCKET_ACTIVE', socketsConnected > 0)
    end
    return true, self.id
end

--- 异步发送数据
-- @number[opt=nil] keepAlive,服务器和客户端最大通信间隔时间,也叫心跳包最大时间,单位秒
-- @string[opt=nil] pingreq,心跳包的字符串
-- @return boole,false 失败，true 表示成功
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
-- while socketClient:asyncSelect() do end
function mt:asyncSelect(keepAlive, pingreq)
    assert(self.co == coroutine.running(), 'socket:asyncSelect: coroutine mismatch')
    if self.error then
        log.warn('socket.client:asyncSelect', 'error', self.error)
        return false
    end

    self.wait = 'SOCKET_SEND'
    local dataLen = 0
    --log.info("socket.asyncSelect #self.output",#self.output)
    while #self.output ~= 0 do
        local data = table.concat(self.output)
        dataLen = string.len(data)
        self.output = {}
        local sendSize = self.protocol == 'UDP' and 1472 or SENDSIZE
        for i = 1, dataLen, sendSize do
            -- 按最大MTU单元对data分包
            socketcore.sock_send(self.id, data:sub(i, i + sendSize - 1))
            if self.timeout then
                self.timerId = sys.timerStart(coroutine.resume, self.timeout * 1000, self.co, false, 'TIMEOUT')
            end
            --log.info("socket.asyncSelect self.timeout",self.timeout)
            local result, reason = coroutine.yield()
            if self.timerId and reason ~= 'TIMEOUT' then
                sys.timerStop(self.timerId)
            end
            sys.publish('SOCKET_ASYNC_SEND', result)
            if not result then
                sys.publish('LIB_SOCKET_SEND_FAIL_IND', self.ssl, self.protocol, self.address, self.port)
                --log.warn('socket.asyncSelect', 'send error')
                return false
            end
        end
    end
    self.wait = 'SOCKET_WAIT'
    --log.info("socket.asyncSelect",dataLen,self.id)
    if dataLen > 0 then
        sys.publish('SOCKET_SEND', self.id, true)
    end
    if keepAlive and keepAlive ~= 0 then
        if type(pingreq) == 'function' then
            sys.timerStart(pingreq, keepAlive * 1000)
        else
            sys.timerStart(self.asyncSend, keepAlive * 1000, self, pingreq or '\0')
        end
    end
    return coroutine.yield()
end

function mt:getAsyncSend()
    if self.error then
        return 0
    end
    return #(self.output)
end
--- 异步缓存待发送的数据
-- @string data 数据
-- @number[opt=nil] timeout 可选参数，发送超时时间，单位秒；为nil时表示不支持timeout
-- @return result true - 成功，false - 失败
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
-- socketClient:asyncSend("12345678")
function mt:asyncSend(data, timeout)
    if self.error then
        log.warn('socket.client:asyncSend', 'error', self.error)
        return false
    end
    self.timeout = timeout
    table.insert(self.output, data or '')
    --log.info("socket.asyncSend",self.wait)
    if self.wait == 'SOCKET_WAIT' then
        coroutine.resume(self.co, true)
    end
    return true
end
--- 异步接收数据
-- @return data 表示接收到的数据(如果是UDP，返回最新的一包数据；如果是TCP,返回所有收到的数据)
--              ""表示未收到数据
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
-- data = socketClient:asyncRecv()
function mt:asyncRecv()
    if #self.input == 0 then
        return ''
    end
    if self.protocol == 'UDP' then
        return table.remove(self.input)
    else
        local s = table.concat(self.input)
        self.input = {}
        if self.isBlock then
            table.insert(self.input, socketcore.sock_recv(self.msg.socket_index, self.msg.recv_len))
        end
        return s
    end
end

--- 同步发送数据
-- @string data 数据
--              此处传入的数据长度和剩余可用内存有关，只要内存够用，可以随便传入数据
--              虽然说此处的数据长度没有特别限制，但是调用core中的socket发送接口时，每次最多发送11200字节的数据
--              例如此处传入的data长度是112000字节，则在这个send接口中，会循环10次，每次发送11200字节的数据
-- @number[opt=120] timeout 可选参数，发送超时时间，单位秒
-- @return result true - 成功，false - 失败
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
-- socketClient:send("12345678")
function mt:send(data, timeout)
    assert(self.co == coroutine.running(), 'socket:send: coroutine mismatch')
    if self.error then
        log.warn('socket.client:send', 'error 异常', self.error)
        return false
    end
    log.debug('socket.send', 'total ' .. string.len(data or '') .. ' bytes', 'first 30 bytes', (data or ''):sub(1, 30))
    local sendSize
    if link.ch395_mac ~= nil then
        sendSize = SENDSIZE_ETH
        for i = 1, string.len(data or ''), sendSize do
            -- 按最大MTU单元对data分包
            self.wait = 'SOCKET_SEND'
            local da = false
            for q = 1, #socket_connect do
                if str2short(socket_connect[q][1] .. '\x00') == self.id then
                    sendETH(self.id, data:sub(i, i + sendSize - 1))
                    da = true
                    break
                end
            end
            if not da then
                log.info('socket:send', 'send fail server not link', false)
                return true
            end
            --socketcore.sock_send(self.id, data:sub(i, i + sendSize - 1))
            self.timerId = sys.timerStart(coroutine.resume, (timeout or 120) * 1000, self.co, false, 'TIMEOUT')
            local result, reason = coroutine.yield()
            if self.timerId and reason ~= 'TIMEOUT' then
                sys.timerStop(self.timerId)
            end
            if not result then
                log.info('socket:send', 'send fail', reason)
                sys.publish('LIB_SOCKET_SEND_FAIL_IND', self.ssl, self.protocol, self.address, self.port)
                return false
            end
        end
        return true
    else
        sendSize = self.protocol == 'UDP' and 1472 or SENDSIZE
        for i = 1, string.len(data or ''), sendSize do
            -- 按最大MTU单元对data分包
            self.wait = 'SOCKET_SEND'
            socketcore.sock_send(self.id, data:sub(i, i + sendSize - 1))
            self.timerId = sys.timerStart(coroutine.resume, (timeout or 120) * 1000, self.co, false, 'TIMEOUT')
            local result, reason = coroutine.yield()
            if self.timerId and reason ~= 'TIMEOUT' then
                sys.timerStop(self.timerId)
            end
            if not result then
                log.info('socket:send', 'send fail', reason)
                sys.publish('LIB_SOCKET_SEND_FAIL_IND', self.ssl, self.protocol, self.address, self.port)
                return false
            end
        end
    end
    return true
end

--- 同步接收数据
-- @number[opt=0] timeout 可选参数，接收超时时间，单位毫秒
-- @string[opt=nil] msg 可选参数，控制socket所在的线程退出recv阻塞状态
-- @bool[opt=nil] msgNoResume 可选参数，控制socket所在的线程退出recv阻塞状态
--                false或者nil表示“在recv阻塞状态，收到msg消息，可以退出阻塞状态”，true表示不退出
--                此参数仅lib内部使用，应用脚本不要使用此参数
-- @return result 数据接收结果
--                true表示成功（接收到了数据）
--                false表示失败（没有接收到数据）
-- @return data
--                如果result为true，data表示接收到的数据(如果是UDP，返回最新的一包数据；如果是TCP,返回所有收到的数据)
--                如果result为false，超时失败，data为"timeout"
--                如果result为false，msg控制退出，data为msg的字符串
--                如果result为false，socket连接被动断开控制退出，data为"CLOSED"
--                如果result为false，PDP断开连接控制退出，data为"IP_ERROR_IND"
-- @return param 如果是msg控制退出，param的值是msg的参数
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
-- result,data = socketClient:recv(60000,"APP_SOCKET_SEND_DATA")
function mt:recv(timeout, msg, msgNoResume)
    assert(self.co == coroutine.running(), 'socket:recv: coroutine mismatch')
    if self.error then
        log.warn('socket.client:recv', 'error', self.error)
        return false
    end
    self.msgNoResume = msgNoResume
    if msg and not self.iSubscribe then
        self.iSubscribe = msg
        self.subMessage = function(data)
            --if data then table.insert(self.output, data) end
            if self.wait == '+RECEIVE' and not self.msgNoResume then
                if data then
                    table.insert(self.output, data)
                end
                coroutine.resume(self.co, 0xAA)
            end
        end
        sys.subscribe(msg, self.subMessage)
    end
    if msg and #self.output > 0 then
        sys.publish(msg, false)
    end
    if #self.input == 0 then
        self.wait = '+RECEIVE'
        if timeout and timeout > 0 then
            local r, s = sys.wait(timeout)
            if r == nil then
                return false, 'timeout'
            elseif r == 0xAA then
                local dat = table.concat(self.output)
                self.output = {}
                return false, msg, dat
            else
                return r, s
            end
        else
            local r, s = coroutine.yield()
            if r == 0xAA then
                local dat = table.concat(self.output)
                self.output = {}
                return false, msg, dat
            else
                return r, s
            end
        end
    end

    if self.protocol == 'UDP' then
        local s = table.remove(self.input)
        return true, s
    else
        log.warn('-------------------使用缓冲区---------------')
        local s = table.concat(self.input)
        self.input = {}
        if self.isBlock then
            table.insert(self.input, socketcore.sock_recv(self.msg.socket_index, self.msg.recv_len))
        end
        return true, s
    end
end

--- 主动关闭并且销毁一个socket
-- @return nil
-- @usage
-- socketClient = socket.tcp()
-- socketClient:connect("www.baidu.com","80")
-- socketClient:close()
function mt:close()
    --assert(self.co == coroutine.running(), "socket:close: coroutine mismatch")
    if self.iSubscribe then
        sys.unsubscribe(self.iSubscribe, self.subMessage)
        self.iSubscribe = false
    end
    --此处不要再判断状态，否则在连接超时失败时，conneted状态仍然是未连接，会导致无法close
    --if self.connected then
    log.info('socket:sock_close', self.id)
    local result, reason

    if self.id then
        if link.ch395_mac ~= nil then
            closeEth(self.id)
        else
            socketcore.sock_close(self.id)
        end

        self.wait = 'SOCKET_CLOSE'
        while true do
            result, reason = coroutine.yield()
            if reason == 'RESPONSE' then
                break
            end
        end
    end
    if self.connected then
        self.connected = false
        if socketsConnected > 0 then
            socketsConnected = socketsConnected - 1
        end
        sys.publish('SOCKET_ACTIVE', socketsConnected > 0)
    end
    if self.input then
        self.input = {}
    end
    --end
    if self.id ~= nil then
        sockets[self.id] = nil
    end
end

-- socket接收自定义控制处理
-- @function[opt=nil] rcvCbFnc，socket接收到数据后，执行的回调函数，回调函数的调用形式为：
-- rcvCbFnc(readFnc,socketIndex,rcvDataLen)
-- rcvCbFnc内部，会判断是否读取数据，如果读取，执行readFnc(socketIndex,rcvDataLen)，返回true；否则返回false或者nil
function mt:setRcvProc(rcvCbFnc)
    assert(self.co == coroutine.running(), 'socket:setRcvProc: coroutine mismatch')
    self.rcvProcFnc = rcvCbFnc
end

function on_response(msg)
    local t = {
        [rtos.MSG_SOCK_CLOSE_CNF] = 'SOCKET_CLOSE',
        --33
        [rtos.MSG_SOCK_SEND_CNF] = 'SOCKET_SEND',
        --32
        [rtos.MSG_SOCK_CONN_CNF] = 'SOCKET_CONNECT'
        --34
    }

    if not sockets[msg.socket_index] then
        log.warn('response on nil socket', msg.socket_index, t[msg.id], msg.result)
        return
    end
    if sockets[msg.socket_index].wait ~= t[msg.id] then
        log.warn(
            'response on invalid wait',
            sockets[msg.socket_index].id,
            sockets[msg.socket_index].wait,
            t[msg.id],
            msg.socket_index
        )
        return
    end
    log.info('socket:on_response:', msg.socket_index, t[msg.id], msg.result)
    if type(socketcore.sock_destroy) == 'function' then
        if (msg.id == rtos.MSG_SOCK_CONN_CNF and msg.result ~= 0) or msg.id == rtos.MSG_SOCK_CLOSE_CNF then
            socketcore.sock_destroy(msg.socket_index)
        end
    end
    coroutine.resume(sockets[msg.socket_index].co, msg.result == 0, 'RESPONSE')
end
sys.subscribe('SOCK_CONN_CNF', on_response)
sys.subscribe('SOCK_CLOSE_CNF', on_response)
sys.subscribe('SOCK_SEND_CN', on_response)
--被动关闭
sys.subscribe(
    'MSG_SOCK_CLOSE_IND',
    function(msg)
        log.info('socket.rtos.MSG_SOCK_CLOSE_IND')
        if not sockets[msg.socket_index] then
            log.warn('close ind on nil socket', msg.socket_index, msg.id)
            return
        end
        if sockets[msg.socket_index].connected then
            sockets[msg.socket_index].connected = false
            if socketsConnected > 0 then
                socketsConnected = socketsConnected - 1
            end
            sys.publish('SOCKET_ACTIVE', socketsConnected > 0)
        end
        sockets[msg.socket_index].error = 'CLOSED'

        --[[
    if type(socketcore.sock_destroy) == "function" then
        socketcore.sock_destroy(msg.socket_index)
    end]]
        sys.publish(
            'LIB_SOCKET_CLOSE_IND',
            sockets[msg.socket_index].ssl,
            sockets[msg.socket_index].protocol,
            sockets[msg.socket_index].address,
            sockets[msg.socket_index].port
        )
        coroutine.resume(sockets[msg.socket_index].co, false, 'CLOSED')
    end
)
sys.subscribe(
    'MSG_SOCK_RECV_IND',
    function(msg)
        if not sockets[msg.socket_index] then
            log.warn('close ind on nil socket', msg.socket_index, msg.id)
            return
        end
        log.debug('socket.recv', msg.recv_len, sockets[msg.socket_index].rcvProcFnc)
        if sockets[msg.socket_index].rcvProcFnc then
            sockets[msg.socket_index].rcvProcFnc(msg.recv_data)
        else
            if sockets[msg.socket_index].wait == '+RECEIVE' then
                coroutine.resume(sockets[msg.socket_index].co, true, msg.recv_data)
            else -- 数据进缓冲区，缓冲区溢出采用覆盖模式
                if #sockets[msg.socket_index].input > INDEX_MAX then
                    log.error('socket recv', 'out of stack', 'block')
                    -- sockets[msg.socket_index].input = {}
                    sockets[msg.socket_index].isBlock = true
                    sockets[msg.socket_index].msg = msg
                else
                    sockets[msg.socket_index].isBlock = false
                    table.insert(sockets[msg.socket_index].input, msg.recv_data)
                end
                sys.publish('SOCKET_RECV', msg.socket_index)
            end
        end
    end
)
--主动关闭
rtos.on(rtos.MSG_SOCK_CLOSE_CNF, on_response)
rtos.on(rtos.MSG_SOCK_CONN_CNF, on_response)
rtos.on(rtos.MSG_SOCK_SEND_CNF, on_response)
--被动关闭
rtos.on(
    rtos.MSG_SOCK_CLOSE_IND,
    function(msg)
        log.info('socket.rtos.MSG_SOCK_CLOSE_IND')
        if not sockets[msg.socket_index] then
            log.warn('close ind on nil socket', msg.socket_index, msg.id)
            return
        end
        if sockets[msg.socket_index].connected then
            sockets[msg.socket_index].connected = false
            if socketsConnected > 0 then
                socketsConnected = socketsConnected - 1
            end
            sys.publish('SOCKET_ACTIVE', socketsConnected > 0)
        end
        sockets[msg.socket_index].error = 'CLOSED'

        --[[
    if type(socketcore.sock_destroy) == "function" then
        socketcore.sock_destroy(msg.socket_index)
    end]]
        sys.publish(
            'LIB_SOCKET_CLOSE_IND',
            sockets[msg.socket_index].ssl,
            sockets[msg.socket_index].protocol,
            sockets[msg.socket_index].address,
            sockets[msg.socket_index].port
        )
        coroutine.resume(sockets[msg.socket_index].co, false, 'CLOSED')
    end
)
--被动接收信息
rtos.on(
    rtos.MSG_SOCK_RECV_IND,
    function(msg)
        if not sockets[msg.socket_index] then
            log.warn('close ind on nil socket', msg.socket_index, msg.id)
            return
        end

        -- local s = socketcore.sock_recv(msg.socket_index, msg.recv_len)
        -- log.debug("socket.recv", "total " .. msg.recv_len .. " bytes", "first " .. 30 .. " bytes", s:sub(1, 30))
        log.debug('socket.recv', msg.recv_len, sockets[msg.socket_index].rcvProcFnc)
        if sockets[msg.socket_index].rcvProcFnc then
            sockets[msg.socket_index].rcvProcFnc(socketcore.sock_recv, msg.socket_index, msg.recv_len)
        else
            if sockets[msg.socket_index].wait == '+RECEIVE' then
                coroutine.resume(
                    sockets[msg.socket_index].co,
                    true,
                    socketcore.sock_recv(msg.socket_index, msg.recv_len)
                )
            else -- 数据进缓冲区，缓冲区溢出采用覆盖模式
                if #sockets[msg.socket_index].input > INDEX_MAX then
                    log.error('socket recv', 'out of stack', 'block')
                    -- sockets[msg.socket_index].input = {}
                    sockets[msg.socket_index].isBlock = true
                    sockets[msg.socket_index].msg = msg
                else
                    sockets[msg.socket_index].isBlock = false
                    table.insert(sockets[msg.socket_index].input, socketcore.sock_recv(msg.socket_index, msg.recv_len))
                end
                sys.publish('SOCKET_RECV', msg.socket_index)
            end
        end
    end
)

--- 设置TCP层自动重传的参数
-- @number[opt=4] retryCnt，重传次数；取值范围0到12
-- @number[opt=16] retryMaxTimeout，限制每次重传允许的最大超时时间(单位秒)，取值范围1到16
-- @return nil
-- @usage
-- setTcpResendPara(3,8)
-- setTcpResendPara(4,16)
function setTcpResendPara(retryCnt, retryMaxTimeout)
    ril.request('AT+TCPUSERPARAM=6,' .. (retryCnt or 4) .. ',7200,' .. (retryMaxTimeout or 16))
end

--- 设置域名解析参数
-- 注意：0027以及之后的core版本才支持此功能
-- @number[opt=4] retryCnt，重传次数；取值范围1到8
-- @number[opt=4] retryTimeoutMulti，重传超时时间倍数，取值范围1到5
--                第n次重传超时时间的计算方式为：第n次的重传超时基数*retryTimeoutMulti，单位为秒
--                重传超时基数表为{1, 1, 2, 4, 4, 4, 4, 4}
--                第1次重传超时时间为：1*retryTimeoutMulti 秒
--                第2次重传超时时间为：1*retryTimeoutMulti 秒
--                第3次重传超时时间为：2*retryTimeoutMulti 秒
--                ...........................................
--                第8次重传超时时间为：8*retryTimeoutMulti 秒
-- @return nil
-- @usage
-- socket.setDnsParsePara(8,5)
function setDnsParsePara(retryCnt, retryTimeoutMulti)
    ril.request('AT*DNSTMOUT=' .. (retryCnt or 4) .. ',' .. (retryTimeoutMulti or 4))
end

--- 打印所有socket的状态
-- @return 无
-- @usage socket.printStatus()
function printStatus()
    for _, client in pairs(sockets) do
        for k, v in pairs(client) do
            log.info('socket.printStatus', 'client', client.id, k, v)
        end
    end
end

--- 设置数据传输后，允许进入休眠状态的延时时长
-- 3024版本以及之后的版本才支持此功能
-- 此功能设置的参数，设置成功后，掉电会自动保存
-- @number tm，数据传输后，允许进入休眠状态的延时时长，单位为秒，取值范围1到20
--             注意：此时间越短，允许进入休眠状态越快，功耗越低；但是在某些网络环境中，此时间越短，可能会造成数据传输不稳定
--                   建议在可以接受的功耗范围内，此值设置的越大越好
--                   如果没有设置此参数，此延时时长是和基站的配置有关，一般来说是10秒左右
-- @return nil
-- @usage
-- socket.setLowPower(5)
function setLowPower(tm)
    ril.request('AT*RTIME=' .. tm)
end

