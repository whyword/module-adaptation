module(..., package.seeall)

local gtab
require 'link'

function str2short(str)
    f, n = pack.unpack(str, '<H')
    return f and f > 0 and n or 0
end
gfunc = function(id, msg, dat)
end
local function shorts(n)
    return pack.pack('<H', n)
end

local function sub_z(data, num)
    return string.sub(data, num + 1, data:len())
end
function ip2num(str)
    if (not (str and #str == 4)) then
        return ''
    end
    local t = {pack.unpack(str, 'bbbb')}
    return string.format('%d.%d.%d.%d', t[2], t[3], t[4], t[5])
end
local function iptostr(ip)
    local d1,d2,d3,d4=string.match(ip,'(%d+)%.(%d+)%.(%d+).(%d+)')
    return {string.sub(shorts(d1),1,1),string.sub(shorts(d2),1,1),string.sub(shorts(d3),1,1),string.sub(shorts(d4),1,1)}
end
--已被连接
socket_connect ={}
--已开启socket数组
socket_on = {}
--未开启socket数组(索引，{IP，端口}，端口，协议类型)
socket_off = {
    {'\x00', {nil, nil}, nil, nil},
    {'\x01', {nil, nil}, nil, nil},
    {'\x02', {nil, nil}, nil, nil},
    {'\x03', {nil, nil}, nil, nil},
    {'\x04', {nil, nil}, nil, nil},
    {'\x05', {nil, nil}, nil, nil},
    {'\x06', {nil, nil}, nil, nil},
    {'\x07', {nil, nil}, nil, nil}
}

--初始化，设置消息中断脚，消息处理函数
function init(t)
    local gpio = t.gpio or pio.P0_7
    gfunc = t.func or function()
        end
    getInputFnc =
        pins.setup(
        gpio,
        function(a)
            if a == 2 then
                log.info("中断")
                wh()
                sys.timerStart(wh, 3000)
            else
                log.info("回复")
            end
        end
    )
end

function wh()
    local a = sub_z(spi.send_recv(spi.SPI_1, '\x19\xff\xff'), 1)
    if bit.isset(str2short(a), 2) then
        -- PHY中断
        spi.send_recv(spi.SPI_1, '\x26\xff')
        local a1 = sub_z(spi.send_recv(spi.SPI_1, '\x26\xff'), 1)
        if a1 == '\x01' then
            gfunc(26, false, 'phy false')
        elseif a1 == '\x08' then
            gfunc(26, true, 'phy true')
        end
    elseif bit.isset(str2short(a), 3) then
        --DHCP中断
        a1 = sub_z(spi.send_recv(spi.SPI_1, '\x42\xff'), 1)
        if a1 == '\x00' then
            gfunc(42, true, 'DHCP true')
        elseif a1 == '\x01' then
            gfunc(42, false, 'DHCP true')
        end
    elseif bit.isset(str2short(a), 0) then
        --不可达中断
        log.info('Unreachable interrupt')
    else
        local a1t = {'sending area nil', 'send succeed', 'rece not nil', 'tcp connect ', 'tcp break', 'Timeout'}
        for i = 4, 11 do
            if bit.isset(str2short(a), i) then
                a1 = sub_z(spi.send_recv(spi.SPI_1, string.char(0x30 , (i - 4) , 0xff ,0xff)), 2)
                for q = 0, 6 do
                    if bit.isset(str2short(a1), q) then
                        gfunc(tonumber('30' .. (i - 4)), true, a1t[q + 1])
                        log.info("看看", tonumber('30' .. (i - 4)),type(q), true, a1t[q + 1],q,q==3)
                        if q == 2 then
                            local data_leng =
                                sub_z(spi.send_recv(spi.SPI_1, string.char(0x3b, (i - 4) ,0xff,0xff)), 2)
                            local data =sub_z(spi.send_recv(spi.SPI_1, string.char(0x3c,i-4)..data_leng..string.rep('\xff',str2short(data_leng))),4)
                            gfunc(tonumber('30' .. (i - 4)), true, data:toHex())
                            spi.send_recv(spi.SPI_1, '\x19\xff\xff')
                            spi.send_recv(spi.SPI_1, string.char(0x30,(i - 4) ,0xff,0xff))
                        end
                        if q == 1 then
                            spi.send_recv(spi.SPI_1, string.char(0x30,(i - 4) ,0xff,0xff))
                            spi.send_recv(spi.SPI_1, '\x19\xff\xff')
                            spi.send_recv(spi.SPI_1, string.char(0x30,(i - 4) ,0xff,0xff))
                            
                        end
                        if q == 3 then
                            log.info("测试连接1",#socket_on)
                            for p=1,#socket_on do
                                log.info("测试连接", str2short(socket_on[p][1]..'\x00'))
                                if str2short(socket_on[p][1]..'\x00')==i-4 then
                                    log.info(i-4, "已连接")
                                    sys.publish('SOCK_CONN_CNF',{
                                        ['socket_index']=i-4,
                                        ['id']=32,
                                        ['result']=0
                                    })
                                    table.insert(socket_connect, socket_on[p])
                                end
                            end
                            sys.publish('go')
                        end
                        if q==4 then
                            for o=1,#socket_connect do
                                if str2short(socket_connect[o][1]..'\x00')==i-4 then
                                    table.remove(socket_connect, o)
                                    for w=1,#socket_on do
                                        if str2short(socket_on[w][1]..'\x00')==i-4 then
                                            sys.publish('SOCK_CLOSE_CNF',{
                                                ['socket_index']=i-4,
                                                ['id']=33,
                                                ['result']=0
                                            })
                                            table.insert(socket_off, table.remove(socket_on, w))
                                            break
                                        end
                                    end
                                    break
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

local a1 = {'42ff', '43ffffffffffffffffffffffffffffffffffffffff'}
--获取当前ch395状态及ip()
function isReady()
    local isReadyt = {{false, ''}, {false, ''},socket_connect,socket_on,socket_off}
    local a2 = string.sub(spi.send_recv(spi.SPI_1, string.fromHex(a1[1])), 2, 2)
    if link.ch395_mac==nil then
        isReadyt[1][1] = false
        isReadyt[1][2] = 'DHCP err'
        return isReadyt
    end
    isReadyt[1][1] = true
    isReadyt[1][2] = 'DHCP succeed'
    a2 = string.sub(spi.send_recv(spi.SPI_1, string.fromHex(a1[2])), 2, 21)
    if string.sub(a2, 1, 4) == '\xff\xff\xff\xff' then
        isReadyt[2][1] = false
        isReadyt[2][2] = 'ip err'
        return isReadyt
    end
    isReadyt[2][1] = true
    isReadyt[2][2] = 'IP:' .. ip2num(string.sub(a2, 1, 4))
    return isReadyt
end


--创建socket(参数)
-- 0.tcp客户端
-- 1.udp客户端
-- 2.ssl(暂不支持)
-- 3.raw 
-- 4.tcp 服务端
function socket(ty,IPC,portC,portS)
    if #socket_off == 0 or not( ty<5 ) then
        return nil, 'no socket'
    end

    -- portS=math.random(0,65535)
    portS=(str2short(socket_off[1][1]..'\x00')+123)*5

    local id=str2short(socket_off[1][1]..'\x00')
    spi.send_recv(spi.SPI_1, '\x19\xff\xff')
    spi.send_recv(spi.SPI_1, '\x30\xff\xff')
    if ty==0 or 4 then
        log.info("测试数据22", ('\x34' .. socket_off[1][1] .. string.char(3)):toHex())
        spi.send_recv(spi.SPI_1, '\x34' .. socket_off[1][1] .. string.char(3))
    elseif ty==1 then
        log.info("测试数据22", ('\x34' .. socket_off[1][1] .. string.char(ty)):toHex())
        spi.send_recv(spi.SPI_1, '\x34' .. socket_off[1][1] .. string.char(ty))
    end
    log.info("测试数据22", ('\x33' .. socket_off[1][1] .. shorts(portS)):toHex())
    spi.send_recv(spi.SPI_1, '\x33' .. socket_off[1][1] .. shorts(portS))
    if ty==0 or 1 then
        log.info("测试数据22", ('\x32' .. socket_off[1][1] .. shorts(tonumber(portC))):toHex())
        spi.send_recv(spi.SPI_1, '\x32' .. socket_off[1][1] .. shorts(tonumber(portC)))
        local ipt=iptostr(IPC)
        log.info("测试数据22", ('\x31' .. socket_off[1][1] .. ipt[1]..ipt[2]..ipt[3]..ipt[4]):toHex())
        spi.send_recv(spi.SPI_1, '\x31' .. socket_off[1][1] .. ipt[1]..ipt[2]..ipt[3]..ipt[4])
        log.info("测试数据22", ('\x35' .. socket_off[1][1]):toHex())
        spi.send_recv(spi.SPI_1, '\x35' .. socket_off[1][1])
        log.info("测试数据22", ('\x37' .. socket_off[1][1]):toHex())
        spi.send_recv(spi.SPI_1, '\x37' .. socket_off[1][1])
    elseif ty==4 then
        spi.send_recv(spi.SPI_1, '\x35' .. socket_off[1][1])
        log.info("测试数据22", ('\x36' .. socket_off[1][1]):toHex())
        spi.send_recv(spi.SPI_1, '\x36' .. socket_off[1][1])
    end
    local aa = sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1)
    if not aa == '\x00' then
        gtab.func()
        return nil
    end
    socket_off[1][3] = port
    socket_off[1][4] = ty
    table.insert(socket_on, table.remove(socket_off, 1))
    log.info("已返回ID", id)
    return id
end

--关闭socket，需重重新初始化
function close(id)
    for o=1,#socket_connect do
        if str2short(socket_connect[o][1]..'\x00')==id then
            spi.send_recv(spi.SPI_1, string.char(0x38,id))
            local a=sub_z(sub_z(spi.send_recv(spi.SPI_1, '\x2c\xff'), 1), 2)
            if a=='\x00' then
                log.info("close socket true")
            end
            table.remove(socket_connect, o)
            for w=1,#socket_on do
                if str2short(socket_on[w][1]..'\x00')==id then
                    table.insert(socket_off, table.remove(socket_on, w))
                    return true ,"close true"
                end
            end
            break
        end
    end
end


--发送数据(参数1.socket id 2.数据)
function send(id, data)
    log.info("发送数据")
    spi.send_recv(
        spi.SPI_1,
        '\x39' .. string.char(id) .. shorts(#data) .. data
    )
    log.info("send data", ('\x39' .. string.char(id)  .. shorts(#data) .. data):toHex() ,data)
    local a1 =
        sub_z(spi.send_recv(spi.SPI_1, string.char(0x30,id,0xff,0xff)), 2)
    if bit.isset(str2short(a1), 0) then
        sys.publish("SOCK_SEND_CN",id)
        return true, 'send true'
    end
    return false, 'send false'
end
